import { NxWelcomeComponent } from './nx-welcome.component';
import { Component } from '@angular/core';
import { HeaderComponent } from '@nx-standalone/ui-share';

@Component({
  standalone: true,
  imports: [NxWelcomeComponent, HeaderComponent],
  selector: 'nx-standalone-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'app-web2';
}
