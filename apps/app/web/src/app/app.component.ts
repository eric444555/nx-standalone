import { NxWelcomeComponent } from './nx-welcome.component';
import { RouterModule } from '@angular/router';
import { Component } from '@angular/core';
import {
  HeaderComponent,
  TestServiceService,
  ToolbarComponent,
} from '@nx-standalone/ui-share';
import { Component2Component } from './component2/component2.component';

@Component({
  standalone: true,
  selector: 'nx-standalone-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [{ provide: TestServiceService, useClass: SystemLogger }],
  imports: [
    NxWelcomeComponent,
    RouterModule,
    HeaderComponent,
    ToolbarComponent,
    Component2Component,
  ],
})
export class AppComponent {
  title = 'app-web';
  getText(text: string) {
    console.log(4444, text);
  }
  constructor(private _testSvc: TestServiceService) {
    console.log('app', this._testSvc.a);
  }
  changeNumber() {
    this._testSvc.changeA = 13132;
    console.log(2222, this._testSvc.a);
  }
}
