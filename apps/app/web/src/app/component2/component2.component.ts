import { Component } from '@angular/core';
import { HeaderComponent, TestServiceService } from '@nx-standalone/ui-share';

@Component({
  standalone: true,
  selector: 'nx-standalone-component2',
  imports: [HeaderComponent],
  templateUrl: './component2.component.html',
  styleUrls: ['./component2.component.css'],
})
export class Component2Component {
  constructor(private _testSvc: TestServiceService) {
    console.log('component2', this._testSvc.a);
  }

  click() {
    console.log(33333, this._testSvc.a);
  }
}
