import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { TestServiceService } from './share-svc/test-service.service';

type HeaderStyleTheme = 'test1' | 'test2';

@Component({
  selector: 'nx-standalone-header',
  standalone: true,
  imports: [CommonModule, MatToolbarModule, MatIconModule, MatButtonModule],
  providers: [TestServiceService],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(private _svc: TestServiceService) {}
  @Input() border: string;
  @Input() color: string;
  @Input() templateStyle: HeaderStyleTheme;
  @ViewChild('button')
  button: ElementRef;

  buttonStyle: object;
  templateClass: HeaderStyleTheme;

  ngOnInit(): void {
    this.buttonStyle = {
      border: this.border,
      color: this.color,
    };

    this.templateClass = this.templateStyle;
    // console.log(this.templateStyle, 333);
  }

  printNumber() {
    console.log(1111, this._svc.a);
  }
  change() {
    this._svc.changeA = 1111;
  }
}
