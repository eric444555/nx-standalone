import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TestServiceService {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private _a = 2;
  constructor() {
    console.log('i am service');
  }

  get a(): number {
    return this._a;
  }

  set changeA(a: number) {
    this._a = a;
  }
}
