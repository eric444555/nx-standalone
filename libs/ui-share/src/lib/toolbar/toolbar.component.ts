import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'nx-standalone-toolbar',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
})
export class ToolbarComponent implements OnInit {
  @Output() outputText = new EventEmitter<string>();
  inputText: string;

  ngOnInit(): void {
    console.log(111);
  }

  click() {
    this.outputText.emit(this.inputText);
  }
}
